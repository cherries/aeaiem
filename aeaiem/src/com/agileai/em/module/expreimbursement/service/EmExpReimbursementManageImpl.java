package com.agileai.em.module.expreimbursement.service;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.em.module.expreimbursement.service.EmExpReimbursementManage;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class EmExpReimbursementManageImpl
        extends StandardServiceImpl
        implements EmExpReimbursementManage {
    public EmExpReimbursementManageImpl() {
        super();
    }
	@Override
	public List<DataRow> findApproveOpinionRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"findApproveOpinionRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public void createApproveRecord(DataParam queryParam) {
		String statementId = sqlNameSpace+"."+"createApproveRecord";
		this.daoHelper.insertRecord(statementId, queryParam);
	}

	@Override
	public void changeStateRecord(DataParam newParam) {
		String statementId = sqlNameSpace+"."+"changeStateRecord";
		this.daoHelper.updateRecord(statementId, newParam);
	}
}
