package com.agileai.em.module.expreimbursement.handler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.em.common.ProcessHelper;
import com.agileai.em.module.expreimbursement.service.EmExpReimbursementManage;
import com.agileai.em.wsproxy.BizAttribute;
import com.agileai.em.wsproxy.ProcessService;
import com.agileai.em.wsproxy.SubmitWorkItem;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.StringUtil;

public class EmExpReimbursementManageApproveHandler
        extends StandardEditHandler {
    public EmExpReimbursementManageApproveHandler() {
        super();
        this.listHandlerClass = EmExpReimbursementManageListHandler.class;
        this.serviceId = buildServiceId(EmExpReimbursementManage.class);
    }
    public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		param.put("ER_ID",param.get("WFIP_BUSINESS_ID"));
		DataRow record = getService().getRecord(param);
		this.setAttributes(record);	
		List<DataRow> records = getService().findApproveOpinionRecords(param);
		setAttribute("ApproceOpinionRecords", records);
		this.setOperaType(operaType);
		setAttributes(param);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    
    protected void processPageAttributes(DataParam param) {
        setAttribute("ER_STYLE",FormSelectFactory.create("ER_STYLE")
        		.addSelectedValue(getAttributeValue("ER_STYLE","")));
        setAttribute("ER_STATE",FormSelectFactory.create("EM_STATE")
        		.addSelectedValue(getAttributeValue("ER_STATE","UNSUBMITTED")));
        setAttribute("EXP_APP_RESULT", FormSelectFactory.create("APPOPERTYPE")
				.addSelectedValue(getAttributeValue("EXP_APP_RESULT", "Y")));
    }
    @PageAction
    public ViewRenderer submit(DataParam param) {
		String responseText = FAIL;
		try {
			User user = (User) getUser();
			DataParam queryParam = new DataParam();
			queryParam.put("EXP_APP_ID",KeyGenerator.instance().genKey());
			queryParam.put("ER_ID",param.get("ER_ID"));
			queryParam.put("EXP_APP_PERSON",user.getUserId());
			queryParam.put("EXP_APP_OPINION",param.get("EXP_APP_OPINION"));
			queryParam.put("EXP_APP_RESULT",param.get("EXP_APP_RESULT"));
			queryParam.put("EXP_APP_TIME",new Date());
			getService().createApproveRecord(queryParam);
			
			BizAttribute bizAttribute = new BizAttribute();
			bizAttribute.setCode("AppoperType");
			bizAttribute.setValue(param.getString("EXP_APP_RESULT"));
			BizAttribute bizAttribute1 = new BizAttribute();
			bizAttribute1.setCode("totalMoney");
			bizAttribute1.setValue(param.get("ER_MONEY"));
			List<BizAttribute> attributeList = new ArrayList<BizAttribute>();
			attributeList.add(bizAttribute);
			attributeList.add(bizAttribute1);
			
			ProcessHelper processHelper = ProcessHelper.instance();
			ProcessService bpmService = processHelper.getBPMService();
			String processId = param.getString("WFP_ID");
			String processInstId = param.getString("WFIP_ID");
			String activityCode = param.getString("WFA_CODE");
			SubmitWorkItem processWorkItem = processHelper.createSubmitWorkItem(processId, processInstId, activityCode);
			if(StringUtil.isNullOrEmpty(attributeList.get(0).getCode())){
				attributeList = processWorkItem.getAttributeList();
			}
			processWorkItem.setUserCode(user.getUserCode());
			processWorkItem.setTitle(param.getString("ER_TITLE"));
			processWorkItem.getAttributeList().addAll(attributeList);
			bpmService.submitProcess(processWorkItem);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		responseText = SUCCESS;
		return new AjaxRenderer(responseText);
	}
    protected EmExpReimbursementManage getService() {
        return (EmExpReimbursementManage) this.lookupService(this.getServiceId());
    }
}
