package com.agileai.em.module.evection.handler;

import com.agileai.domain.DataParam;
import com.agileai.em.module.evection.service.QueryProListSelect;
import com.agileai.hotweb.controller.core.PickFillModelHandler;

public class QueryProListSelectListHandler
        extends PickFillModelHandler {
    public QueryProListSelectListHandler() {
        super();
        this.serviceId = buildServiceId(QueryProListSelect.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "pcName", "");
    }

    protected QueryProListSelect getService() {
        return (QueryProListSelect) this.lookupService(this.getServiceId());
    }
}
