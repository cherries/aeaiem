package com.agileai.em.module.evection.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.em.common.ProcessHelper;
import com.agileai.em.module.evection.service.EmEvectionManage;
import com.agileai.em.wsproxy.BizAttribute;
import com.agileai.em.wsproxy.HandleWorkItem;
import com.agileai.em.wsproxy.ProcessService;
import com.agileai.em.wsproxy.SubmitWorkItem;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.bizmoduler.core.MasterSubService;
import com.agileai.hotweb.controller.core.MasterSubEditMainHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class EmEvectionManageAgainEditHandler
        extends MasterSubEditMainHandler {
	public static final String PROCESS_CODE = "EM_EVECTION";
    public EmEvectionManageAgainEditHandler() {
        super();
        this.listHandlerClass = EmEvectionManageListHandler.class;
        this.serviceId = buildServiceId(EmEvectionManage.class);
        this.baseTablePK = "EVE_ID";
        this.defaultTabId = "_base";
    }
    public ViewRenderer prepareDisplay(DataParam param) {
		param.put("EVE_ID",param.get("WFIP_BUSINESS_ID"));
		DataRow record = getService().getMasterRecord(param);
		this.setAttributes(record);			
		List<DataRow> records = getService().findApproveOpinionRecords(param);
		setAttribute("ApproceOpinionRecords", records);
		String currentSubTableId = param.get("currentSubTableId",defaultTabId);
		if (!currentSubTableId.equals(MasterSubService.BASE_TABLE_ID)){
			String subRecordsKey = currentSubTableId+"Records";
			if (!this.getAttributesContainer().containsKey(subRecordsKey)){
				List<DataRow> subRecords = getService().findSubRecords(currentSubTableId, param);
				this.setAttribute(currentSubTableId+"Records", subRecords);
			}
		}
		this.setAttribute("currentSubTableId", currentSubTableId);
		this.setAttribute("currentSubTableIndex", getTabIndex(currentSubTableId));
		String operateType = param.get(OperaType.KEY);
		this.setOperaType(operateType);
		setAttributes(param);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    
    protected void processPageAttributes(DataParam param) {
        setAttribute("EVE_STATE",FormSelectFactory.create("EM_STATE")
        		.addSelectedValue(getOperaAttributeValue("EVE_STATE","UNSUBMITTED")));
        initMappingItem("EVP_TRANSPORTATION_WAY",
                FormSelectFactory.create("TRANSPORTATION_WAY").getContent());
    }

    protected String[] getEntryEditFields(String currentSubTableId) {
        List<String> temp = new ArrayList<String>();
        return temp.toArray(new String[] {  });
    }
    
    @PageAction
    public ViewRenderer submit(DataParam param) {
    	User user = (User) getUser();
		String responseText = "fail";
		param.put("EVE_STATE", "AUDITING");
		getService().updateMasterRecord(param);
		saveSubRecords(param);
		responseText = param.get(baseTablePK);
		String masterRecordId = param.get("EVE_ID");
		this.getService().computeTotalMoney(masterRecordId);
		
		List<BizAttribute> attributeList = new ArrayList<BizAttribute>();
		String processId = param.getString("WFP_ID");
		String processInstId = param.getString("WFIP_ID");
		String activityCode = param.getString("WFA_CODE");
		
		ProcessHelper processHelper = ProcessHelper.instance();
		ProcessService bpmService = processHelper.getBPMService();
		SubmitWorkItem processWorkItem = processHelper.createSubmitWorkItem(processId, processInstId, activityCode);
		attributeList = processWorkItem.getAttributeList();
		processWorkItem.setUserCode(user.getUserCode());
		processWorkItem.getAttributeList().addAll(attributeList);
		bpmService.submitProcess(processWorkItem);
		responseText = SUCCESS;
		return new AjaxRenderer(responseText);
	}
    
    @PageAction
    public ViewRenderer cancel(DataParam param)throws Exception {
		String responseText = "fail";
		User user = (User) getUser();
		param.put("EVE_STATE", "CANCEL");
		getService().updateMasterRecord(param);
		ProcessHelper processHelper = ProcessHelper.instance();
   		ProcessService processService = processHelper.getBPMService();
		String processId = param.getString("WFP_ID");
		String processInstId = param.getString("WFIP_ID");
		
		HandleWorkItem handleWorkItem = new HandleWorkItem();
		handleWorkItem.setProcessId(processId);
		handleWorkItem.setProcessInstanceId(processInstId);
		handleWorkItem.setUserCode(user.getUserCode());
		processService.terminateProcess(handleWorkItem);
		responseText = SUCCESS;
		return new AjaxRenderer(responseText);
	}
    
    protected String getEntryEditTablePK(String currentSubTableId) {
        HashMap<String, String> primaryKeys = new HashMap<String, String>();
        primaryKeys.put("EmExpenses", "EXP_ID");
        return primaryKeys.get(currentSubTableId);
    }
    
    protected String getEntryEditForeignKey(String currentSubTableId) {
        HashMap<String, String> foreignKeys = new HashMap<String, String>();
        foreignKeys.put("EmExpenses", "EVE_ID");
        return foreignKeys.get(currentSubTableId);
    }
    
    protected EmEvectionManage getService() {
        return (EmEvectionManage) this.lookupService(this.getServiceId());
    }
}
