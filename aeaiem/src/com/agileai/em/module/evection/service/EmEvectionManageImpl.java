package com.agileai.em.module.evection.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.MasterSubServiceImpl;
import com.agileai.util.StringUtil;

public class EmEvectionManageImpl
        extends MasterSubServiceImpl
        implements EmEvectionManage {
    public EmEvectionManageImpl() {
        super();
    }

    public String[] getTableIds() {
        List<String> temp = new ArrayList<String>();

        temp.add("_base");
        temp.add("EmExpenses");

        return temp.toArray(new String[] {  });
    }
    @Override
	public void computeTotalMoney(String masterRecordId) {
		String statementId = sqlNameSpace + "." + "getMasterRecord";
		DataParam param = new DataParam("EVE_ID", masterRecordId);
		DataRow masterRecord = this.daoHelper.getRecord(statementId, param);

		BigDecimal eve_subsidy = (BigDecimal) masterRecord.get("EVE_SUBSIDY");
		if (eve_subsidy == null) {
			eve_subsidy = new BigDecimal("0.00");
		}

		statementId = sqlNameSpace + "." + "getTotalGatherRecord";
		DataRow gatherRecord = this.daoHelper.getRecord(statementId, param);

		BigDecimal gather_transportation_fee = (BigDecimal) gatherRecord
				.get("GATHER_TRANSPORTATION_FEE");
		if (gather_transportation_fee == null) {
			gather_transportation_fee = new BigDecimal("0.00");
		}
		BigDecimal gather_expe_other = (BigDecimal) gatherRecord
				.get("GATHER_EXPE_OTHER");
		if (gather_expe_other == null) {
			gather_expe_other = new BigDecimal("0.00");
		}
		BigDecimal gather_expe_hotel = (BigDecimal) gatherRecord
				.get("GATHER_EXPE_HOTEL");
		if (gather_expe_hotel == null) {
			gather_expe_hotel = new BigDecimal("0.00");
		}
		
		BigDecimal eve_total_money = eve_subsidy.add(gather_transportation_fee)
				.add(gather_expe_other).add(gather_expe_hotel);
		
		statementId = sqlNameSpace + "." + "updateMasterGatherRecord";
		DataParam updateParam = new DataParam();
		updateParam.put("EVE_ID", masterRecordId, "EVE_TOTAL_MONEY",
				eve_total_money);
		this.daoHelper.updateRecord(statementId, updateParam);

	}

	@Override
	public List<DataRow> findApproveOpinionRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"findApproveOpinionRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public void createApproveRecord(DataParam queryParam) {
		String statementId = sqlNameSpace+"."+"createApproveRecord";
		this.daoHelper.insertRecord(statementId, queryParam);
	}

	@Override
	public void changeStateRecord(DataParam newParam) {
		String statementId = sqlNameSpace+"."+"changeStateRecord";
		this.daoHelper.updateRecord(statementId, newParam);
		
	}

	@Override
	public List<DataRow> findPcNameRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"findPcNameRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
	
	@Override
	public void createMasterRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"insertMasterRecord";
		processDataType(param, tableName);
		if(StringUtil.isNullOrEmpty(param.getString("EVE_ID"))){
			processPrimaryKeys(tableName,param);			
		}
		this.daoHelper.insertRecord(statementId, param);
	}

	@Override
	public List<DataRow> mobileFindEvectionRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"findMasterRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public DataRow mobileGetEvectionRecord(DataParam param) {
		String masterRecordId = param.getString("EVE_ID");
		this.computeTotalMoney(masterRecordId);
		
		String statementId = sqlNameSpace+"."+"getMasterRecord";
		DataRow result = this.daoHelper.getRecord(statementId, param);
		return result;
	}

	@Override
	public void createEvectionRecord(DataParam createParam) {
		String statementId = sqlNameSpace+"."+"insertMasterRecord";
		processDataType(createParam, tableName);
		if(StringUtil.isNullOrEmpty(createParam.getString("EVE_ID"))){
			processPrimaryKeys(tableName,createParam);			
		}
		this.daoHelper.insertRecord(statementId, createParam);
	}

	@Override
	public void updateEvectionRecord(DataParam updateParam) {
		String statementId = sqlNameSpace+"."+"updateMasterRecord";
		processDataType(updateParam, tableName);
		this.daoHelper.updateRecord(statementId, updateParam);
	}

	@Override
	public void deleteEvectionRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"deleteMasterRecord";
		this.daoHelper.deleteRecords(statementId, param);
		String[] tableIds = this.getTableIds();
		for (int i=0;i < tableIds.length;i++){
			String tableId = tableIds[i];
			if (BASE_TABLE_ID.equals(tableId))continue;
			statementId = sqlNameSpace+"."+"delete"+StringUtil.upperFirst(tableId)+"Records";
			this.daoHelper.deleteRecords(statementId, param);
		}
	}

	@Override
	public List<DataRow> findExpRecords(DataParam param) {
		List<DataRow> result = null;
		String statementId = sqlNameSpace+"."+"find"+StringUtil.upperFirst("EmExpenses")+"Records";
		result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public DataRow getExpRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"get"+StringUtil.upperFirst("EmExpenses")+"Record";
		DataRow result = this.daoHelper.getRecord(statementId, param);
		
		statementId = sqlNameSpace+"."+"getMasterRecord";
		DataParam newParam = new DataParam("EVE_ID",result.getString("EVE_ID"));
		DataRow eveResult = this.daoHelper.getRecord(statementId, newParam);
		result.put("state", eveResult.getString("EVE_STATE"));
		return result;
	}

	@Override
	public void createExpRecord(DataParam createParam) {
		String subId = "EmExpenses";
		String curTableName = subTableIdNameMapping.get(subId);
		processDataType(createParam, curTableName);	
		processPrimaryKeys(curTableName,createParam);
		processSubSortField(subId,createParam);
		String statementId = sqlNameSpace+"."+"insert"+StringUtil.upperFirst(subId)+"Record";
		this.daoHelper.insertRecord(statementId, createParam);
	}

	@Override
	public void updateExpRecord(DataParam updateParam) {
		String subId = "EmExpenses";
		String statementId = sqlNameSpace+"."+"update"+StringUtil.upperFirst(subId)+"Record";
		String curTableName = subTableIdNameMapping.get(subId);
		processDataType(updateParam, curTableName);
		this.daoHelper.updateRecord(statementId, updateParam);
	}

	@Override
	public void deleteExpRecord(DataParam param) {
		String subId = "EmExpenses";
		String statementId = sqlNameSpace+"."+"delete"+StringUtil.upperFirst(subId)+"Record";
		this.daoHelper.deleteRecords(statementId, param);
	}

	@Override
	public List<DataRow> findEmPayDiagramRecords(DataParam param) {
		List<DataRow> result = null;
		String statementId = sqlNameSpace+"."+"findEmPayRecords";
		result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public List<DataRow> findEmPayPersonalData(String userId) {
		List<DataRow> result = null;
		DataParam param = new DataParam();
		param.put("userId", userId);
		String statementId = sqlNameSpace+"."+"findEmPayPersonalData";
		result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public DataRow getEmPayPersonalData(String id) {
		String statementId = sqlNameSpace+".getEmPayPersonalData";
		DataParam param = new DataParam("id",id);
		DataRow result = this.daoHelper.getRecord(statementId, param);
		return result;
	}

	@Override
	public List<DataRow> findAppList(String id) {
		String statementId = sqlNameSpace+"."+"findAppList";
		DataParam param = new DataParam("BizId",id);
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public List<DataRow> getEmPersonalPayDiagramData(String userId) {
		String statementId = sqlNameSpace+"."+"getEmPersonalPayDiagramData";
		DataParam param = new DataParam("userId",userId);
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public DataRow getTaskEveRecord(String id) {
		String statementId = sqlNameSpace+".getTaskEveRecord";
		DataParam param = new DataParam("id",id);
		DataRow result = new DataRow();
		result = this.daoHelper.getRecord(statementId, param);
		return result;
	}

	@Override
	public DataRow getTaskExpreRecord(String id) {
		String statementId = sqlNameSpace+".getTaskExpreRecord";
		DataParam param = new DataParam("id",id);
		DataRow result = new DataRow();
		result = this.daoHelper.getRecord(statementId, param);
		return result;
	}
}
