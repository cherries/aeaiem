package com.agileai.em.module.proconf.service;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface EmProConfigManage
        extends StandardService {
	 public DataRow showNumRecord(DataParam param) ;
}
