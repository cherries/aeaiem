package com.agileai.em.module.proconf.handler;

import java.util.Date;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.em.common.PrivilegeHelper;
import com.agileai.em.module.proconf.service.EmProConfigManage;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class EmProConfigManageListHandler
        extends StandardListHandler {
    public EmProConfigManageListHandler() {
        super();
        this.editHandlerClazz = EmProConfigManageEditHandler.class;
        this.serviceId = buildServiceId(EmProConfigManage.class);
    }
    public ViewRenderer prepareDisplay(DataParam param){
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		User user = (User) this.getUser();
		PrivilegeHelper privilegeHelper = new PrivilegeHelper(user);
		if(privilegeHelper.isManager()){
			setAttribute("showEdit",true);
			setAttribute("showDel",true);
		}
		if(privilegeHelper.isProleader()){
			setAttribute("showEdit",true);
			setAttribute("showDel",true);
		}
		List<DataRow> rsList = getService().findRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    protected void processPageAttributes(DataParam param) {
    	initMappingItem("PC_STYLE",
                FormSelectFactory.create("PC_STYLE").getContent());
    	initMappingItem("PC_STATE",
                FormSelectFactory.create("PC_STATE").getContent());
    	setAttribute("pcState",FormSelectFactory.create("PC_STATE")
        		.addSelectedValue(param.get("pcState")));
    }
    
    protected void initParameters(DataParam param) {
        initParamItem(param, "pcCode", "");
        initParamItem(param, "pcName", "");
        initParamItem(param, "pcState", "");
        initParamItem(param,"sdate",DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(DateUtil.getBeginOfYear(new Date()),DateUtil.YEAR,-1)));
		initParamItem(param,"edate",DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, 1)));
    }

    protected EmProConfigManage getService() {
        return (EmProConfigManage) this.lookupService(this.getServiceId());
    }
}
