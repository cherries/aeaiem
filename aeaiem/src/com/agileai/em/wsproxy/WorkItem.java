
package com.agileai.em.wsproxy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for workItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="workItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="activityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="activityName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="businessId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="businessURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="laucheTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="laucherId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="laucherName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mobileBizURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="procInstanceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="procInstanceState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="processId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="processName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recordId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="submiteTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="submiterId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="submiterName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "workItem", propOrder = {
    "activityCode",
    "activityName",
    "businessId",
    "businessURL",
    "laucheTime",
    "laucherId",
    "laucherName",
    "mobileBizURL",
    "procInstanceId",
    "procInstanceState",
    "processId",
    "processName",
    "recordId",
    "submiteTime",
    "submiterId",
    "submiterName",
    "title"
})
public class WorkItem {

    protected String activityCode;
    protected String activityName;
    protected String businessId;
    protected String businessURL;
    protected String laucheTime;
    protected String laucherId;
    protected String laucherName;
    protected String mobileBizURL;
    protected String procInstanceId;
    protected String procInstanceState;
    protected String processId;
    protected String processName;
    protected String recordId;
    protected String submiteTime;
    protected String submiterId;
    protected String submiterName;
    protected String title;

    /**
     * Gets the value of the activityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityCode() {
        return activityCode;
    }

    /**
     * Sets the value of the activityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityCode(String value) {
        this.activityCode = value;
    }

    /**
     * Gets the value of the activityName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityName() {
        return activityName;
    }

    /**
     * Sets the value of the activityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityName(String value) {
        this.activityName = value;
    }

    /**
     * Gets the value of the businessId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessId() {
        return businessId;
    }

    /**
     * Sets the value of the businessId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessId(String value) {
        this.businessId = value;
    }

    /**
     * Gets the value of the businessURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessURL() {
        return businessURL;
    }

    /**
     * Sets the value of the businessURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessURL(String value) {
        this.businessURL = value;
    }

    /**
     * Gets the value of the laucheTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLaucheTime() {
        return laucheTime;
    }

    /**
     * Sets the value of the laucheTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLaucheTime(String value) {
        this.laucheTime = value;
    }

    /**
     * Gets the value of the laucherId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLaucherId() {
        return laucherId;
    }

    /**
     * Sets the value of the laucherId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLaucherId(String value) {
        this.laucherId = value;
    }

    /**
     * Gets the value of the laucherName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLaucherName() {
        return laucherName;
    }

    /**
     * Sets the value of the laucherName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLaucherName(String value) {
        this.laucherName = value;
    }

    /**
     * Gets the value of the mobileBizURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobileBizURL() {
        return mobileBizURL;
    }

    /**
     * Sets the value of the mobileBizURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobileBizURL(String value) {
        this.mobileBizURL = value;
    }

    /**
     * Gets the value of the procInstanceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcInstanceId() {
        return procInstanceId;
    }

    /**
     * Sets the value of the procInstanceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcInstanceId(String value) {
        this.procInstanceId = value;
    }

    /**
     * Gets the value of the procInstanceState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcInstanceState() {
        return procInstanceState;
    }

    /**
     * Sets the value of the procInstanceState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcInstanceState(String value) {
        this.procInstanceState = value;
    }

    /**
     * Gets the value of the processId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessId() {
        return processId;
    }

    /**
     * Sets the value of the processId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessId(String value) {
        this.processId = value;
    }

    /**
     * Gets the value of the processName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessName() {
        return processName;
    }

    /**
     * Sets the value of the processName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessName(String value) {
        this.processName = value;
    }

    /**
     * Gets the value of the recordId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordId() {
        return recordId;
    }

    /**
     * Sets the value of the recordId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordId(String value) {
        this.recordId = value;
    }

    /**
     * Gets the value of the submiteTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubmiteTime() {
        return submiteTime;
    }

    /**
     * Sets the value of the submiteTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubmiteTime(String value) {
        this.submiteTime = value;
    }

    /**
     * Gets the value of the submiterId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubmiterId() {
        return submiterId;
    }

    /**
     * Sets the value of the submiterId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubmiterId(String value) {
        this.submiterId = value;
    }

    /**
     * Gets the value of the submiterName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubmiterName() {
        return submiterName;
    }

    /**
     * Sets the value of the submiterName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubmiterName(String value) {
        this.submiterName = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

}
