
package com.agileai.em.wsproxy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for submitProcess complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="submitProcess">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="submitWorkItem" type="{http://www.agileai.com/bpm/api}submitWorkItem" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "submitProcess", propOrder = {
    "submitWorkItem"
})
public class SubmitProcess {

    protected SubmitWorkItem submitWorkItem;

    /**
     * Gets the value of the submitWorkItem property.
     * 
     * @return
     *     possible object is
     *     {@link SubmitWorkItem }
     *     
     */
    public SubmitWorkItem getSubmitWorkItem() {
        return submitWorkItem;
    }

    /**
     * Sets the value of the submitWorkItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmitWorkItem }
     *     
     */
    public void setSubmitWorkItem(SubmitWorkItem value) {
        this.submitWorkItem = value;
    }

}
