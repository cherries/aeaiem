
package com.agileai.em.wsproxy;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.agileai.em.wsproxy package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ResumeProcessResponse_QNAME = new QName("http://www.agileai.com/bpm/api", "resumeProcessResponse");
    private final static QName _SaveDraft_QNAME = new QName("http://www.agileai.com/bpm/api", "saveDraft");
    private final static QName _GetProcessInstanceStateResponse_QNAME = new QName("http://www.agileai.com/bpm/api", "getProcessInstanceStateResponse");
    private final static QName _FindWorkItems_QNAME = new QName("http://www.agileai.com/bpm/api", "findWorkItems");
    private final static QName _RetrieveProcess_QNAME = new QName("http://www.agileai.com/bpm/api", "retrieveProcess");
    private final static QName _GetCurrentProcessId_QNAME = new QName("http://www.agileai.com/bpm/api", "getCurrentProcessId");
    private final static QName _DelegateProcess_QNAME = new QName("http://www.agileai.com/bpm/api", "delegateProcess");
    private final static QName _TerminateProcessResponse_QNAME = new QName("http://www.agileai.com/bpm/api", "terminateProcessResponse");
    private final static QName _SubmitProcess_QNAME = new QName("http://www.agileai.com/bpm/api", "submitProcess");
    private final static QName _SubmitProcessResponse_QNAME = new QName("http://www.agileai.com/bpm/api", "submitProcessResponse");
    private final static QName _GetProcessId8InstId_QNAME = new QName("http://www.agileai.com/bpm/api", "getProcessId8InstId");
    private final static QName _TerminateProcess_QNAME = new QName("http://www.agileai.com/bpm/api", "terminateProcess");
    private final static QName _SuspendProcess_QNAME = new QName("http://www.agileai.com/bpm/api", "suspendProcess");
    private final static QName _InvokeProcess_QNAME = new QName("http://www.agileai.com/bpm/api", "invokeProcess");
    private final static QName _SaveDraftResponse_QNAME = new QName("http://www.agileai.com/bpm/api", "saveDraftResponse");
    private final static QName _GetProcessInstanceState_QNAME = new QName("http://www.agileai.com/bpm/api", "getProcessInstanceState");
    private final static QName _IsOwnTaskResponse_QNAME = new QName("http://www.agileai.com/bpm/api", "isOwnTaskResponse");
    private final static QName _FindWorkItemsResponse_QNAME = new QName("http://www.agileai.com/bpm/api", "findWorkItemsResponse");
    private final static QName _LaunchProcess_QNAME = new QName("http://www.agileai.com/bpm/api", "launchProcess");
    private final static QName _DelegateProcessResponse_QNAME = new QName("http://www.agileai.com/bpm/api", "delegateProcessResponse");
    private final static QName _SuspendProcessResponse_QNAME = new QName("http://www.agileai.com/bpm/api", "suspendProcessResponse");
    private final static QName _ResumeProcess_QNAME = new QName("http://www.agileai.com/bpm/api", "resumeProcess");
    private final static QName _GetCurrentProcessIdResponse_QNAME = new QName("http://www.agileai.com/bpm/api", "getCurrentProcessIdResponse");
    private final static QName _InvokeProcessResponse_QNAME = new QName("http://www.agileai.com/bpm/api", "invokeProcessResponse");
    private final static QName _GetProcessId8InstIdResponse_QNAME = new QName("http://www.agileai.com/bpm/api", "getProcessId8InstIdResponse");
    private final static QName _IsOwnTask_QNAME = new QName("http://www.agileai.com/bpm/api", "isOwnTask");
    private final static QName _LaunchProcessResponse_QNAME = new QName("http://www.agileai.com/bpm/api", "launchProcessResponse");
    private final static QName _RetrieveProcessResponse_QNAME = new QName("http://www.agileai.com/bpm/api", "retrieveProcessResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.agileai.em.wsproxy
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetProcessInstanceState }
     * 
     */
    public GetProcessInstanceState createGetProcessInstanceState() {
        return new GetProcessInstanceState();
    }

    /**
     * Create an instance of {@link IsOwnTaskResponse }
     * 
     */
    public IsOwnTaskResponse createIsOwnTaskResponse() {
        return new IsOwnTaskResponse();
    }

    /**
     * Create an instance of {@link FindWorkItemsResponse }
     * 
     */
    public FindWorkItemsResponse createFindWorkItemsResponse() {
        return new FindWorkItemsResponse();
    }

    /**
     * Create an instance of {@link SaveDraftResponse }
     * 
     */
    public SaveDraftResponse createSaveDraftResponse() {
        return new SaveDraftResponse();
    }

    /**
     * Create an instance of {@link InvokeProcess }
     * 
     */
    public InvokeProcess createInvokeProcess() {
        return new InvokeProcess();
    }

    /**
     * Create an instance of {@link SuspendProcess }
     * 
     */
    public SuspendProcess createSuspendProcess() {
        return new SuspendProcess();
    }

    /**
     * Create an instance of {@link TerminateProcess }
     * 
     */
    public TerminateProcess createTerminateProcess() {
        return new TerminateProcess();
    }

    /**
     * Create an instance of {@link SubmitProcess }
     * 
     */
    public SubmitProcess createSubmitProcess() {
        return new SubmitProcess();
    }

    /**
     * Create an instance of {@link SubmitProcessResponse }
     * 
     */
    public SubmitProcessResponse createSubmitProcessResponse() {
        return new SubmitProcessResponse();
    }

    /**
     * Create an instance of {@link GetProcessId8InstId }
     * 
     */
    public GetProcessId8InstId createGetProcessId8InstId() {
        return new GetProcessId8InstId();
    }

    /**
     * Create an instance of {@link DelegateProcess }
     * 
     */
    public DelegateProcess createDelegateProcess() {
        return new DelegateProcess();
    }

    /**
     * Create an instance of {@link TerminateProcessResponse }
     * 
     */
    public TerminateProcessResponse createTerminateProcessResponse() {
        return new TerminateProcessResponse();
    }

    /**
     * Create an instance of {@link SaveDraft }
     * 
     */
    public SaveDraft createSaveDraft() {
        return new SaveDraft();
    }

    /**
     * Create an instance of {@link GetProcessInstanceStateResponse }
     * 
     */
    public GetProcessInstanceStateResponse createGetProcessInstanceStateResponse() {
        return new GetProcessInstanceStateResponse();
    }

    /**
     * Create an instance of {@link FindWorkItems }
     * 
     */
    public FindWorkItems createFindWorkItems() {
        return new FindWorkItems();
    }

    /**
     * Create an instance of {@link RetrieveProcess }
     * 
     */
    public RetrieveProcess createRetrieveProcess() {
        return new RetrieveProcess();
    }

    /**
     * Create an instance of {@link GetCurrentProcessId }
     * 
     */
    public GetCurrentProcessId createGetCurrentProcessId() {
        return new GetCurrentProcessId();
    }

    /**
     * Create an instance of {@link ResumeProcessResponse }
     * 
     */
    public ResumeProcessResponse createResumeProcessResponse() {
        return new ResumeProcessResponse();
    }

    /**
     * Create an instance of {@link LaunchProcessResponse }
     * 
     */
    public LaunchProcessResponse createLaunchProcessResponse() {
        return new LaunchProcessResponse();
    }

    /**
     * Create an instance of {@link RetrieveProcessResponse }
     * 
     */
    public RetrieveProcessResponse createRetrieveProcessResponse() {
        return new RetrieveProcessResponse();
    }

    /**
     * Create an instance of {@link IsOwnTask }
     * 
     */
    public IsOwnTask createIsOwnTask() {
        return new IsOwnTask();
    }

    /**
     * Create an instance of {@link GetProcessId8InstIdResponse }
     * 
     */
    public GetProcessId8InstIdResponse createGetProcessId8InstIdResponse() {
        return new GetProcessId8InstIdResponse();
    }

    /**
     * Create an instance of {@link ResumeProcess }
     * 
     */
    public ResumeProcess createResumeProcess() {
        return new ResumeProcess();
    }

    /**
     * Create an instance of {@link GetCurrentProcessIdResponse }
     * 
     */
    public GetCurrentProcessIdResponse createGetCurrentProcessIdResponse() {
        return new GetCurrentProcessIdResponse();
    }

    /**
     * Create an instance of {@link InvokeProcessResponse }
     * 
     */
    public InvokeProcessResponse createInvokeProcessResponse() {
        return new InvokeProcessResponse();
    }

    /**
     * Create an instance of {@link SuspendProcessResponse }
     * 
     */
    public SuspendProcessResponse createSuspendProcessResponse() {
        return new SuspendProcessResponse();
    }

    /**
     * Create an instance of {@link LaunchProcess }
     * 
     */
    public LaunchProcess createLaunchProcess() {
        return new LaunchProcess();
    }

    /**
     * Create an instance of {@link DelegateProcessResponse }
     * 
     */
    public DelegateProcessResponse createDelegateProcessResponse() {
        return new DelegateProcessResponse();
    }

    /**
     * Create an instance of {@link DelegateWorkItem }
     * 
     */
    public DelegateWorkItem createDelegateWorkItem() {
        return new DelegateWorkItem();
    }

    /**
     * Create an instance of {@link ResultStatus }
     * 
     */
    public ResultStatus createResultStatus() {
        return new ResultStatus();
    }

    /**
     * Create an instance of {@link SubmitWorkItem }
     * 
     */
    public SubmitWorkItem createSubmitWorkItem() {
        return new SubmitWorkItem();
    }

    /**
     * Create an instance of {@link WorkItem }
     * 
     */
    public WorkItem createWorkItem() {
        return new WorkItem();
    }

    /**
     * Create an instance of {@link HandleWorkItem }
     * 
     */
    public HandleWorkItem createHandleWorkItem() {
        return new HandleWorkItem();
    }

    /**
     * Create an instance of {@link Activity }
     * 
     */
    public Activity createActivity() {
        return new Activity();
    }

    /**
     * Create an instance of {@link BizAttribute }
     * 
     */
    public BizAttribute createBizAttribute() {
        return new BizAttribute();
    }

    /**
     * Create an instance of {@link Process }
     * 
     */
    public Process createProcess() {
        return new Process();
    }

    /**
     * Create an instance of {@link LaunchWorkItem }
     * 
     */
    public LaunchWorkItem createLaunchWorkItem() {
        return new LaunchWorkItem();
    }

    /**
     * Create an instance of {@link InvokeWorkItem }
     * 
     */
    public InvokeWorkItem createInvokeWorkItem() {
        return new InvokeWorkItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResumeProcessResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "resumeProcessResponse")
    public JAXBElement<ResumeProcessResponse> createResumeProcessResponse(ResumeProcessResponse value) {
        return new JAXBElement<ResumeProcessResponse>(_ResumeProcessResponse_QNAME, ResumeProcessResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDraft }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "saveDraft")
    public JAXBElement<SaveDraft> createSaveDraft(SaveDraft value) {
        return new JAXBElement<SaveDraft>(_SaveDraft_QNAME, SaveDraft.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessInstanceStateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "getProcessInstanceStateResponse")
    public JAXBElement<GetProcessInstanceStateResponse> createGetProcessInstanceStateResponse(GetProcessInstanceStateResponse value) {
        return new JAXBElement<GetProcessInstanceStateResponse>(_GetProcessInstanceStateResponse_QNAME, GetProcessInstanceStateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindWorkItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "findWorkItems")
    public JAXBElement<FindWorkItems> createFindWorkItems(FindWorkItems value) {
        return new JAXBElement<FindWorkItems>(_FindWorkItems_QNAME, FindWorkItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveProcess }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "retrieveProcess")
    public JAXBElement<RetrieveProcess> createRetrieveProcess(RetrieveProcess value) {
        return new JAXBElement<RetrieveProcess>(_RetrieveProcess_QNAME, RetrieveProcess.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrentProcessId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "getCurrentProcessId")
    public JAXBElement<GetCurrentProcessId> createGetCurrentProcessId(GetCurrentProcessId value) {
        return new JAXBElement<GetCurrentProcessId>(_GetCurrentProcessId_QNAME, GetCurrentProcessId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DelegateProcess }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "delegateProcess")
    public JAXBElement<DelegateProcess> createDelegateProcess(DelegateProcess value) {
        return new JAXBElement<DelegateProcess>(_DelegateProcess_QNAME, DelegateProcess.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TerminateProcessResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "terminateProcessResponse")
    public JAXBElement<TerminateProcessResponse> createTerminateProcessResponse(TerminateProcessResponse value) {
        return new JAXBElement<TerminateProcessResponse>(_TerminateProcessResponse_QNAME, TerminateProcessResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitProcess }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "submitProcess")
    public JAXBElement<SubmitProcess> createSubmitProcess(SubmitProcess value) {
        return new JAXBElement<SubmitProcess>(_SubmitProcess_QNAME, SubmitProcess.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitProcessResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "submitProcessResponse")
    public JAXBElement<SubmitProcessResponse> createSubmitProcessResponse(SubmitProcessResponse value) {
        return new JAXBElement<SubmitProcessResponse>(_SubmitProcessResponse_QNAME, SubmitProcessResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessId8InstId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "getProcessId8InstId")
    public JAXBElement<GetProcessId8InstId> createGetProcessId8InstId(GetProcessId8InstId value) {
        return new JAXBElement<GetProcessId8InstId>(_GetProcessId8InstId_QNAME, GetProcessId8InstId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TerminateProcess }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "terminateProcess")
    public JAXBElement<TerminateProcess> createTerminateProcess(TerminateProcess value) {
        return new JAXBElement<TerminateProcess>(_TerminateProcess_QNAME, TerminateProcess.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SuspendProcess }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "suspendProcess")
    public JAXBElement<SuspendProcess> createSuspendProcess(SuspendProcess value) {
        return new JAXBElement<SuspendProcess>(_SuspendProcess_QNAME, SuspendProcess.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvokeProcess }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "invokeProcess")
    public JAXBElement<InvokeProcess> createInvokeProcess(InvokeProcess value) {
        return new JAXBElement<InvokeProcess>(_InvokeProcess_QNAME, InvokeProcess.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDraftResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "saveDraftResponse")
    public JAXBElement<SaveDraftResponse> createSaveDraftResponse(SaveDraftResponse value) {
        return new JAXBElement<SaveDraftResponse>(_SaveDraftResponse_QNAME, SaveDraftResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessInstanceState }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "getProcessInstanceState")
    public JAXBElement<GetProcessInstanceState> createGetProcessInstanceState(GetProcessInstanceState value) {
        return new JAXBElement<GetProcessInstanceState>(_GetProcessInstanceState_QNAME, GetProcessInstanceState.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsOwnTaskResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "isOwnTaskResponse")
    public JAXBElement<IsOwnTaskResponse> createIsOwnTaskResponse(IsOwnTaskResponse value) {
        return new JAXBElement<IsOwnTaskResponse>(_IsOwnTaskResponse_QNAME, IsOwnTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindWorkItemsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "findWorkItemsResponse")
    public JAXBElement<FindWorkItemsResponse> createFindWorkItemsResponse(FindWorkItemsResponse value) {
        return new JAXBElement<FindWorkItemsResponse>(_FindWorkItemsResponse_QNAME, FindWorkItemsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LaunchProcess }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "launchProcess")
    public JAXBElement<LaunchProcess> createLaunchProcess(LaunchProcess value) {
        return new JAXBElement<LaunchProcess>(_LaunchProcess_QNAME, LaunchProcess.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DelegateProcessResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "delegateProcessResponse")
    public JAXBElement<DelegateProcessResponse> createDelegateProcessResponse(DelegateProcessResponse value) {
        return new JAXBElement<DelegateProcessResponse>(_DelegateProcessResponse_QNAME, DelegateProcessResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SuspendProcessResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "suspendProcessResponse")
    public JAXBElement<SuspendProcessResponse> createSuspendProcessResponse(SuspendProcessResponse value) {
        return new JAXBElement<SuspendProcessResponse>(_SuspendProcessResponse_QNAME, SuspendProcessResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResumeProcess }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "resumeProcess")
    public JAXBElement<ResumeProcess> createResumeProcess(ResumeProcess value) {
        return new JAXBElement<ResumeProcess>(_ResumeProcess_QNAME, ResumeProcess.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrentProcessIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "getCurrentProcessIdResponse")
    public JAXBElement<GetCurrentProcessIdResponse> createGetCurrentProcessIdResponse(GetCurrentProcessIdResponse value) {
        return new JAXBElement<GetCurrentProcessIdResponse>(_GetCurrentProcessIdResponse_QNAME, GetCurrentProcessIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvokeProcessResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "invokeProcessResponse")
    public JAXBElement<InvokeProcessResponse> createInvokeProcessResponse(InvokeProcessResponse value) {
        return new JAXBElement<InvokeProcessResponse>(_InvokeProcessResponse_QNAME, InvokeProcessResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProcessId8InstIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "getProcessId8InstIdResponse")
    public JAXBElement<GetProcessId8InstIdResponse> createGetProcessId8InstIdResponse(GetProcessId8InstIdResponse value) {
        return new JAXBElement<GetProcessId8InstIdResponse>(_GetProcessId8InstIdResponse_QNAME, GetProcessId8InstIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsOwnTask }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "isOwnTask")
    public JAXBElement<IsOwnTask> createIsOwnTask(IsOwnTask value) {
        return new JAXBElement<IsOwnTask>(_IsOwnTask_QNAME, IsOwnTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LaunchProcessResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "launchProcessResponse")
    public JAXBElement<LaunchProcessResponse> createLaunchProcessResponse(LaunchProcessResponse value) {
        return new JAXBElement<LaunchProcessResponse>(_LaunchProcessResponse_QNAME, LaunchProcessResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveProcessResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.agileai.com/bpm/api", name = "retrieveProcessResponse")
    public JAXBElement<RetrieveProcessResponse> createRetrieveProcessResponse(RetrieveProcessResponse value) {
        return new JAXBElement<RetrieveProcessResponse>(_RetrieveProcessResponse_QNAME, RetrieveProcessResponse.class, null, value);
    }

}
