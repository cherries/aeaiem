
package com.agileai.em.wsproxy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for delegateProcess complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="delegateProcess">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="delegateWorkItem" type="{http://www.agileai.com/bpm/api}delegateWorkItem" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "delegateProcess", propOrder = {
    "delegateWorkItem"
})
public class DelegateProcess {

    protected DelegateWorkItem delegateWorkItem;

    /**
     * Gets the value of the delegateWorkItem property.
     * 
     * @return
     *     possible object is
     *     {@link DelegateWorkItem }
     *     
     */
    public DelegateWorkItem getDelegateWorkItem() {
        return delegateWorkItem;
    }

    /**
     * Sets the value of the delegateWorkItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link DelegateWorkItem }
     *     
     */
    public void setDelegateWorkItem(DelegateWorkItem value) {
        this.delegateWorkItem = value;
    }

}
