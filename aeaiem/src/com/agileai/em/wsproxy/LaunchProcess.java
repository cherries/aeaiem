
package com.agileai.em.wsproxy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for launchProcess complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="launchProcess">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="launchWorkItem" type="{http://www.agileai.com/bpm/api}launchWorkItem" minOccurs="0"/>
 *         &lt;element name="skipFirstNode" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "launchProcess", propOrder = {
    "launchWorkItem",
    "skipFirstNode"
})
public class LaunchProcess {

    protected LaunchWorkItem launchWorkItem;
    protected boolean skipFirstNode;

    /**
     * Gets the value of the launchWorkItem property.
     * 
     * @return
     *     possible object is
     *     {@link LaunchWorkItem }
     *     
     */
    public LaunchWorkItem getLaunchWorkItem() {
        return launchWorkItem;
    }

    /**
     * Sets the value of the launchWorkItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link LaunchWorkItem }
     *     
     */
    public void setLaunchWorkItem(LaunchWorkItem value) {
        this.launchWorkItem = value;
    }

    /**
     * Gets the value of the skipFirstNode property.
     * 
     */
    public boolean isSkipFirstNode() {
        return skipFirstNode;
    }

    /**
     * Sets the value of the skipFirstNode property.
     * 
     */
    public void setSkipFirstNode(boolean value) {
        this.skipFirstNode = value;
    }

}
