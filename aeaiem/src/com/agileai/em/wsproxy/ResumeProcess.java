
package com.agileai.em.wsproxy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for resumeProcess complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="resumeProcess">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="handleWorkItem" type="{http://www.agileai.com/bpm/api}handleWorkItem" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resumeProcess", propOrder = {
    "handleWorkItem"
})
public class ResumeProcess {

    protected HandleWorkItem handleWorkItem;

    /**
     * Gets the value of the handleWorkItem property.
     * 
     * @return
     *     possible object is
     *     {@link HandleWorkItem }
     *     
     */
    public HandleWorkItem getHandleWorkItem() {
        return handleWorkItem;
    }

    /**
     * Sets the value of the handleWorkItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link HandleWorkItem }
     *     
     */
    public void setHandleWorkItem(HandleWorkItem value) {
        this.handleWorkItem = value;
    }

}
