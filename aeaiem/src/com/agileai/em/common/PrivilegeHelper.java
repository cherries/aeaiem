package com.agileai.em.common;

import java.util.List;

import com.agileai.hotweb.domain.core.Role;
import com.agileai.hotweb.domain.core.User;
import com.agileai.util.CryptionUtil;

public class PrivilegeHelper {
	private User user = null;
	public PrivilegeHelper(User user){
		this.user = user;
}
	public boolean isFinance(){
		boolean result = false;
		List<Role> roleList = user.getRoleList();
		for (int i=0;i < roleList.size();i++){
			Role role = roleList.get(i);
			if ("Finance".equals(role.getRoleCode())){
				result = true;
				break;
			}
		}
		return result;
	}
	public boolean isManager(){
		boolean result = false;
		List<Role> roleList = user.getRoleList();
		for (int i=0;i < roleList.size();i++){
			Role role = roleList.get(i);
			if ("Manager".equals(role.getRoleCode())){
				result = true;
				break;
			}
		}
		return result;
	}
	public boolean isCashier(){
		boolean result = false;
		List<Role> roleList = user.getRoleList();
		for (int i=0;i < roleList.size();i++){
			Role role = roleList.get(i);
			if ("Cashier".equals(role.getRoleCode())){
				result = true;
				break;
			}
		}
		return result;
	}
	
	public boolean isProleader(){
		boolean result = false;
		List<Role> roleList = user.getRoleList();
		for (int i=0;i < roleList.size();i++){
			Role role = roleList.get(i);
			if ("Proleader".equals(role.getRoleCode())){
				result = true;
				break;
			}
		}
		return result;
	}
	public static void main(String[] args) {
		String aa = CryptionUtil.encryption("123456", "12345678");
		System.out.println(aa);
	}
}