<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveRecord(){
	if (!validate()){
		return;
	}
	showSplash();
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
		if (responseText == 'success'){
			hideSplash();
			parent.PopupBox.closeCurrent();
			parent.refreshPage();
		}
	}});
}
function interChange(){
	var departure = $('#EXP_DEPARTURE').val();
	var destination = $('#EXP_DESTINATION').val(); 
	$('#EXP_DEPARTURE').val(destination);
	$('#EXP_DESTINATION').val(departure);
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveRecord()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>出发地-目的地</th>
	<td><input id="EXP_DEPARTURE" label="出发地点" defaulttext="请输入出发地点" name="EXP_DEPARTURE" type="text" value="<%=pageBean.inputValue("EXP_DEPARTURE")%>" size="24" class="text" />
		<input value="&nbsp;" type="button" class="interChangeImgBtn" id="interChangeImgBtn" title="互换" onclick="interChange()"/>
		<input id="EXP_DESTINATION" label="目的地点" defaulttext="请输入目的地点" name="EXP_DESTINATION" type="text" value="<%=pageBean.inputValue("EXP_DESTINATION")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>出发时间</th>
	<td><input id="EXP_START_TIME" label="出发时间" name="EXP_START_TIME" type="text" value="<%=pageBean.inputTime("EXP_START_TIME")%>" size="24" class="text" /><img id="EXP_START_TIMEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
<tr>
	<th width="100" nowrap>到达时间</th>
	<td><input id="EXP_END_TIME" label="到达时间" name="EXP_END_TIME" type="text" value="<%=pageBean.inputTime("EXP_END_TIME")%>" size="24" class="text" /><img id="EXP_END_TIMEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
<tr>
	<th width="100" nowrap>交通方式</th>
	<td>
	<select id="EXP_TRANSPORTATION_WAY" label="交通方式" name="EXP_TRANSPORTATION_WAY" class="select"><%=pageBean.selectValue("EXP_TRANSPORTATION_WAY")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>交通费用</th>
	<td><input id="EXP_TRANSPORTATION_FEE" label="交通费用" name="EXP_TRANSPORTATION_FEE" type="text" value="<%=pageBean.inputValue("EXP_TRANSPORTATION_FEE")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>住宿费用</th>
	<td><input id="EXP_HOREL_FEE" label="住宿费用" name="EXP_HOREL_FEE" type="text" value="<%=pageBean.inputValue("EXP_HOREL_FEE")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>其他费用</th>
	<td><input id="EXP_OTHER_FEE" label="其他费用" name="EXP_OTHER_FEE" type="text" value="<%=pageBean.inputValue("EXP_OTHER_FEE")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>备注</th>
	<td>
	<textarea id="EXP_REMARKS" label="备注" name="EXP_REMARKS" cols="60" rows="5" class="textarea"><%=pageBean.inputValue("EXP_REMARKS")%></textarea>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="EXP_ID" name="EXP_ID" value="<%=pageBean.inputValue4DetailOrUpdate("EXP_ID","")%>" />
<input type="hidden" id="EVE_ID" name="EVE_ID" value="<%=pageBean.inputValue("EVE_ID")%>" />
</form>
<script language="javascript">
initCalendar('EXP_START_TIME','%Y-%m-%d %H:%M','EXP_START_TIMEPicker');
initCalendar('EXP_END_TIME','%Y-%m-%d %H:%M','EXP_END_TIMEPicker');
requiredValidator.add("EXP_START_TIME");
datetimeValidators[0].set("yyyy-MM-dd HH:mm").add("EXP_START_TIME");
requiredValidator.add("EXP_END_TIME");
datetimeValidators[1].set("yyyy-MM-dd HH:mm").add("EXP_END_TIME");
numValidator.add("EXP_TRANSPORTATION_FEE");
numValidator.add("EXP_HOREL_FEE");
numValidator.add("EXP_OTHER_FEE");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
